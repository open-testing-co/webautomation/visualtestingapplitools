package com.handresc1127.utils;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BaseTests {

    protected static WebDriver driver;
    protected static EyesManager eyesManager;

    @BeforeClass
    public static void beforeAll(){
        PropertyLoader.loadProperties();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        eyesManager = new EyesManager(driver, "TAU - Visual Testing Applitools");
    }

    @AfterClass
    public static void afterAll(){
        driver.quit();
        eyesManager.abort();
    }

}
