package com.handresc1127;

import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.selenium.Eyes;
import com.handresc1127.utils.BaseTests;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class SearchTest extends BaseTests
{

    @BeforeClass
    public static void startVisualTestSuite(){
        eyesManager.setBatchName("Mixelaneus");
        eyesManager.setForceFullScreen(true);
    }

    @Test
    public void searchingHenryCorreaOnGoogle()
    {
        driver.get("https://www.google.com");
        WebElement inputField=driver.findElement(By.xpath("//input[@name='q']"));
        inputField.sendKeys("Henry Andres Correa Correa");
        inputField.sendKeys(Keys.RETURN);



        Eyes eyes= eyesManager.getEyes();
        eyes.open(driver,"Mixelaneus","searchingHenryCorreaOnGoogle");
        eyes.checkWindow();
        eyes.checkElement(By.xpath("//img[contains(@title,'linkedin.com/in/henry-andr')]"));
        eyes.close();
    }

    @Test
    public void dynamicContent()
    {
        driver.get("https://the-internet.herokuapp.com/dynamic_content");
        eyesManager.validateWindows(MatchLevel.LAYOUT);
    }
}
